package com.example.demo.Services;

import java.util.List;

import com.example.demo.Models.Makers;
import com.example.demo.Models.TypeMaker;

public interface IMakersService {
	List<Makers> findByName(String nom);
    Makers findByIdMakers(int id);
    List<Makers> getMakersByTypeWithoutCorporations(TypeMaker typemaker);
    List<String> getMakersByTypeWithCorporations(TypeMaker typemaker, int idCorporation);


}
