package com.example.demo.Services;

import com.example.demo.Models.ResultatFinal;

public interface IGamesService {
	ResultatFinal isEndGame();

	ResultatFinal setWinnerGame();

}
