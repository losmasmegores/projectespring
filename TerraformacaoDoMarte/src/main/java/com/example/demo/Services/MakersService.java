package com.example.demo.Services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Models.Makers;
import com.example.demo.Models.TypeMaker;
import com.example.demo.Repositories.MakersRepository;

@Service("MakerService")
public class MakersService implements IMakersService{

    @Autowired
    MakersRepository mr;

    public List<Makers> findAll() {
        return mr.findAll();
    }

    @Override
    public List<Makers> findByName(String nom) {
        return mr.findByName(nom);
    }

    @Override
    public Makers findByIdMakers(int id) {
        return mr.findById(id);
    }


    public List<String> getMakersByTypeWithoutCorporationString(TypeMaker typemaker) {
        List<Makers> makers = mr.findAll();
        List<String> res = new ArrayList<>();
        for (Makers m : makers) {
            if (m.getTypeMaker() == typemaker && m.getCorporation() == null) res.add(m.toString());
        }
        return res;
    }



	@Override
	public List<Makers> getMakersByTypeWithoutCorporations(TypeMaker typemaker) {
		  List<Makers> makers = mr.findAll();
	        List<Makers> res = new ArrayList<>();
	        for (Makers m : makers) {
	            if (m.getTypeMaker() == typemaker && m.getCorporation() == null) res.add(m);
	        }
	        System.out.println(res);
	        return res;
	}

	@Override
	public List<String> getMakersByTypeWithCorporations(TypeMaker typemaker, int idCorporation) {
		   List<Makers> makers = mr.findAll();
	        List<String> res = new ArrayList<>();
	        for (Makers m : makers) {
	            if (m.getCorporation() != null && m.getCorporation().getId() == idCorporation) res.add(m.toString());
	        }
	        return res;
	}

}
