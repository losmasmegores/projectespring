package com.example.demo.Services;

import com.example.demo.Models.Dices;
import com.example.demo.Models.ResultatFinal;

public interface IDicesService {
	ResultatFinal resolveDices(Dices dices);
}
