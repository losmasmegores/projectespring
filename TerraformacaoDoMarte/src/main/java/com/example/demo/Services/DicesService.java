package com.example.demo.Services;

import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Models.Dices;
import com.example.demo.Models.Games;
import com.example.demo.Models.ResultatFinal;
import com.example.demo.Repositories.GamesRepository;

@Service("DicesService")
public class DicesService implements IDicesService {

	@Autowired
	GamesRepository gr;

	@Override
	public ResultatFinal resolveDices(Dices dices) {

		Games game = gr.findAll().get(0);

		double temp = game.getTemp();
		int oxy = game.getOxy();
		int ocea = game.getOceans();
		int ciudad = 0;
		int bosque = 0;
		int vicPoint = 0;

		String StringFinal = "";
		
		for (Entry<Integer, Integer> entry : dices.getResultTirades().entrySet()) {

			if (entry.getValue() >= 3) {
				switch (entry.getKey()) {
				case 1:
					temp++;
					break;
				case 2:
					oxy++;
					break;
				case 3:
					ocea++;
					break;
				case 4:
					bosque++;
					break;
				case 5:
					ciudad++;
					break;
				case 6:
					vicPoint++;
					break;
				default:
					break;
				}
			}
		}
		   gr.save(game);
	        if (temp >= 3) {
	        	StringFinal += "La temperatura sube 2 grados ";
	        }
	        if (oxy >= 3) {
	        	StringFinal += "Ha subido el oxigeno ";
	        }
	        if (ocea >= 3) {
	        	StringFinal += "Una corporacion tiene 3 casillas de oceano ";
	        }
	        if (bosque >= 3) {
	        	StringFinal += "Una corporacion tiene 3 casillas de bosque";
	        }
	        if (ciudad >= 3) {
	        	StringFinal += "Una corporacion tiene 3 casillas de ciudad";
	        }
	        if (vicPoint >= 3) {
	        	StringFinal += "Ganas dos puntos de victoria";
	        }

		return new ResultatFinal(StringFinal);

	}

}
