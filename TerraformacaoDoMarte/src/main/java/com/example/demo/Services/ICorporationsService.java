package com.example.demo.Services;

import java.util.List;

import com.example.demo.Models.Corporations;

public interface ICorporationsService {
	Corporations findByIdCorporations(int id);

	List<Corporations> findByName(String nom);

	List<String> getCorporationsWithPlayer();

	List<String> setVictoryPointsMakers();

}
