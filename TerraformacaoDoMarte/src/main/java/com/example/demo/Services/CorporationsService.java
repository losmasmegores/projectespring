package com.example.demo.Services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Models.Corporations;
import com.example.demo.Repositories.CorporationsRepository;

@Service("CorporationsService")
public class CorporationsService implements ICorporationsService {

	@Autowired
	CorporationsRepository cr;

	public List<Corporations> findAll() {
		return cr.findAll();
	}

	@Override
	public List<Corporations> findByName(String nom) {
		return cr.findByName(nom);
	}

	@Override
	public Corporations findByIdCorporations(int id) {
		return cr.findById(id);
	}

	@Override
	public List<String> setVictoryPointsMakers() {
		List<Corporations> corps = cr.findAll();
		List<String> corpVictoryPoint = new ArrayList<>();
		for(Corporations c : corps) {
			c.setVicPoints(c.getMakers().size() * 2);
			corpVictoryPoint.add("Corporacio: " + c.getName() +", VictoryPoints: " + c.getVicPoints());
		}
		cr.saveAll(corps);
		return corpVictoryPoint;
	}

	@Override
	public List<String> getCorporationsWithPlayer() {
		List<Corporations> corporations = cr.findAll();
		System.out.println(corporations);
		List<String> corpPlayers = new ArrayList<>();
		for (int i = 0; i < corporations.size(); i++) {
			if (corporations.get(i).getPlayers() != null) {
				corpPlayers.add(corporations.get(i).toString());
			}
		}
		return corpPlayers;
	}


}
