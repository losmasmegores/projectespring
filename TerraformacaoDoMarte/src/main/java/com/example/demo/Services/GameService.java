package com.example.demo.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Models.Corporations;
import com.example.demo.Models.Games;
import com.example.demo.Models.GamesWins;
import com.example.demo.Models.Makers;
import com.example.demo.Models.ResultatFinal;
import com.example.demo.Models.TypeMaker;
import com.example.demo.Repositories.GamesRepository;

@Service("GameService")
public class GameService implements IGamesService  {

	@Autowired
    GamesRepository gr;
    @Autowired
    MakersService ms;
    @Autowired
    CorporationsService cs;
	@Override
	public ResultatFinal isEndGame() {
		Games a = gr.findAll().get(0);
		int condicions = 0;
		System.out.println(a.getTemp()+"TEMP");
		if(a.getTemp() > 0.1) {
			condicions++;
		}
		if(a.getOxy() > 13) {
			System.out.println("AADFAWFJIKAIOFJIOSEJUFIOAWEJFIOJAWFIOJAIFJIAJKIFJKAWLEJFKAJWÑFJAIOFJIOAHJFHIO");
			condicions++;
		}
		boolean si = false;
		for (Makers m : ms.findAll()) {
            if (m.getTypeMaker() == TypeMaker.OCEA && m.getCorporation() != null) {
                si = true;
                break;
            }
        }
		if (si) {
			condicions++;
		}

		if(condicions > 1) {
			System.out.println("return true");
			return new ResultatFinal("Se acabo el juego");
		}
		else {

			return new ResultatFinal("El juego sigue");
		}
	}
	@Override
	public ResultatFinal setWinnerGame() {

		Games a = gr.findAll().get(0);
        List<Corporations> Corpiss = cs.findAll();
        int maximo = -100000;
        Corporations gano = null;
        for (Corporations corporations : Corpiss) {
			if (corporations.getVicPoints()>maximo) {
				maximo = corporations.getVicPoints();
				gano = corporations;
			}
		}
        GamesWins winn = new GamesWins(a, gano.getPlayers());
	
		return new ResultatFinal("Ha nagado "+gano.getPlayers());
	}




}
