package com.example.demo.Controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Models.Dices;
import com.example.demo.Models.ResultatFinal;
import com.example.demo.Models.TypeMaker;
import com.example.demo.Services.CorporationsService;
import com.example.demo.Services.DicesService;
import com.example.demo.Services.GameService;
import com.example.demo.Services.MakersService;

@RestController
public class Controller {

    @Autowired
    CorporationsService cs;

    @Autowired
    MakersService ms;

    @Autowired
    GameService gs;

    @Autowired
    DicesService ds;

    @GetMapping(path = "/")
    public String hello() {
        return "Spring Funciona ";
    }

    @GetMapping(path = "/test")
    public String test(@RequestParam String a, @RequestParam String b) {
        return "hola " + a + " y "+b;
    }

    //localhost:8080/hello2param?nomAlumne=Manuel&nomAlumne2=Dario
    @GetMapping(path = "/hello2param")
    public String hello(@RequestParam String nomAlumne, @RequestParam String nomAlumne2) {
        return "hola " + nomAlumne + " y " + nomAlumne2;
    }

    @GetMapping(path = "/getMakersByTypeWithoutCorporation")
    public List<String> getMakersByTypeWithoutCorporationString(@RequestParam TypeMaker typemaker) {
        return ms.getMakersByTypeWithoutCorporationString(typemaker);
    }

    @GetMapping(path = "/getCorporationsWithPlayer")
    public List<String> getCorporationsWithPlayer() {
        return cs.getCorporationsWithPlayer();
    }

    @GetMapping(path = "/getMakersByTypeWithCorporation")
    public List<String> getMakersByTypeWithCorporation(@RequestParam TypeMaker typemaker, @RequestParam int idCorporation) {
        return ms.getMakersByTypeWithCorporations(typemaker, idCorporation);
    }

    @GetMapping(path = "/rollingDices")
    public HashMap<Integer, Integer> rollingDices() {
    	Random r = new Random();
		HashMap<Integer,Integer> Dices = new HashMap<Integer,Integer>();
		for(int i = 0; i < 6; i++) {
			int n = r.nextInt(6)+1;
			Dices.put(i+1, n);
		}
		return Dices;
    }

    @PostMapping(path = "/resolveDices")
    private ResultatFinal resolveDices(@RequestBody Dices dices) {
        return ds.resolveDices(dices);
    }

    @GetMapping(path = "/isEndedGame")
    public ResultatFinal isEndedGame() {
        return gs.isEndGame();
    }

    @GetMapping(path = "/setVictoryPointsMakers")
    public List<String> setVictoryPointsMakers() {
        return cs.setVictoryPointsMakers();
    }

    @GetMapping(path = "/setWinnerGame")
    public ResultatFinal setWinnerGame() {
        return gs.setWinnerGame();
    }
}
