package com.example.demo.Models;
import java.util.Date;
import java.util.HashSet;

import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.JoinColumn;




@Entity
@Table(name = "games")
public class Games {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idGame")
	private int id;

	@Column(name = "oxygen")
	private int oxy = 0;
	
	@Column(name = "temperature")
	private double temp = -30;
	
	@Column(name = "oceans")
	private int oceans = 0;
	
	@Column(name = "dateStart")
	private Date dateStart;
	
	@Column(name = "dateEnd")
	private Date dateEnd;
	
	@OneToOne(mappedBy = "games", cascade = CascadeType.PERSIST)
    private GamesWins gamesWins;
	
	@ManyToMany(fetch = FetchType.EAGER) 
	@JoinTable(
			name="Partidas", 
			joinColumns = @JoinColumn(name="id_Partida"), 
			inverseJoinColumns = @JoinColumn(name="id_Jugador")
	)
	private Set<Players> jugadors = new HashSet<Players>();
	
	//CONSTRUCTORS
	
	public Games() {
		super();
	}

	
	
	public Games(int oxy, double temp, int oceans, Date dateStart, Date dateEnd) {
		super();
		this.oxy = oxy;
		this.temp = temp;
		this.oceans = oceans;
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
	}



	public Games(int oxy, double temp, int oceans, Date dateStart, Date dateEnd, GamesWins gamesWins,Set<Players> jugadors) {
		super();
		this.oxy = oxy;
		this.temp = temp;
		this.oceans = oceans;
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
		this.gamesWins = gamesWins;
		this.jugadors = jugadors;
	}

	
	//GETTES && SETTERS
	
	public int getOxy() {
		return oxy;
	}

	public void setOxy(int oxy) {
		this.oxy = oxy;
	}



	public double getTemp() {
		return temp;
	}

	public void setTemp(double temp) {
		this.temp = temp;
	}



	public int getOceans() {
		return oceans;
	}

	public void setOceans(int oceans) {
		this.oceans = oceans;
	}



	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}



	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}



	public GamesWins getGamesWins() {
		return gamesWins;
	}

	public void setGamesWins(GamesWins gamesWins) {
		this.gamesWins = gamesWins;
	}


	
	public Set<Players> getJugadors() {
		return jugadors;
	}
	
	public void setJugadors(Set<Players> jugadors) {
		this.jugadors = jugadors;
	}

	public void addJugadors(Players jugadors) {
		this.jugadors.add(jugadors);
	}



	public int getId() {
		return id;
	}
}