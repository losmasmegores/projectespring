package com.example.demo.Models;

import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;

import jakarta.persistence.Table;

@Entity
@Table(name = "makers")
public class Makers {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idMakers")
	private int id;

	@Column(name = "name", length = 50, nullable = false)
	private String name;

	@Column(name = "maxNeighbours")
	private int maxNeigh = 0;

	@Enumerated(EnumType.STRING)
	@Column(name = "typeMaker", nullable = false)
	private TypeMaker typeMaker;

	@ManyToOne
	@JoinColumn(name = "Corporation")
	private Corporations corporation;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "Vecinos", joinColumns = @JoinColumn(name = "id_Maker"), inverseJoinColumns = @JoinColumn(name = "id_MakerVei"))
	private Set<Makers> vecinos = new HashSet<Makers>();

	// CONSTRUCTORES
	public Makers() {
		super();
	}

	public Makers(String name, int maxNeigh, TypeMaker typeMaker) {
		super();
		this.name = name;
		this.maxNeigh = maxNeigh;
		this.typeMaker = typeMaker;
	}

	public Makers(String name, int maxNeigh, TypeMaker typeMaker, Corporations corporation, Set<Makers> vecinos) {
		super();
		this.name = name;
		this.maxNeigh = maxNeigh;
		this.typeMaker = typeMaker;
		this.corporation = corporation;
		this.vecinos = vecinos;
	}

	// GETTERS && SETTERS

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMaxNeigh() {
		return maxNeigh;
	}

	public void setMaxNeigh(int maxNeigh) {
		this.maxNeigh = maxNeigh;
	}

	public TypeMaker getTypeMaker() {
		return typeMaker;
	}

	public void setTypeMaker(TypeMaker typeMaker) {
		this.typeMaker = typeMaker;
	}

	public Corporations getCorporation() {
		return corporation;
	}

	public void setCorporation(Corporations corporation) {
		this.corporation = corporation;
	}

	public Set<Makers> getVecinos() {
		return vecinos;
	}

	public void setVecinos(Set<Makers> vecinos) {
		this.vecinos = vecinos;
	}

	public void addVecinos(Makers vecino) {
		this.vecinos.add(vecino);
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Makers [id=" + id + ", name=" + name + ", maxNeigh=" + maxNeigh + ", typeMaker=" + typeMaker
				+ ", corporation=" + corporation + ", vecinos=" + vecinos + "]";
	}
}
