package com.example.demo.Models;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "gamesWins")
public class GamesWins {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idGamesWins")
	private int id;

	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.REMOVE })
	@JoinColumn(name = "idGame")
	private Games games;

	@ManyToOne
	@JoinColumn(name = "idPlayer")
	private Players players;

	// CONSTRUCTORS

	public GamesWins() {
		super();
	}

	public GamesWins(Games games, Players players) {
		super();
		this.games = games;
		this.players = players;
	}

	// GETTERS && SETTERS

	public Games getGames() {
		return games;
	}

	public void setGames(Games games) {
		this.games = games;
	}

	public Players getPlayers() {
		return players;
	}

	public void setPlayers(Players players) {
		this.players = players;
	}

	public int getId() {
		return id;
	}
}
