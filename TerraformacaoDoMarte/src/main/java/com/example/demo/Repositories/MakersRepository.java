package com.example.demo.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Models.Makers;

public interface MakersRepository extends JpaRepository<Makers, Integer> {

	List<Makers> findByName(String nom);
    Makers findById(int id);

}
