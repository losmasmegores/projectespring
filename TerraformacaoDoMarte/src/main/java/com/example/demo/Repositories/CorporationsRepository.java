package com.example.demo.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.Models.Corporations;

@Repository
public interface CorporationsRepository extends JpaRepository<Corporations, Integer> {
	List<Corporations> findByName(String nom);
	Corporations findById(int id);

}
